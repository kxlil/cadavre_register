const express = require('express')
const app = express()
const cors = require('cors')
const logger = require('morgan')

const port  = process.env.PORT || '3000'
const host = process.env.HOST || 'localhost'

app.use(express.json())
app.use(cors())
app.use(logger('tiny'))

// list des providers
const providers = []

app.get('/providers', (req, res) => {
    const type = req.query.type

    if(!type) return res.sendStatus(404)

    res.status(200).json(
        providers.filter(obj => obj.type === type)
    )
})

app.put('/providers', (req, res) => {
    const provider = req.body

    if(!provider) return res.sendStatus(400)

    if(registered(provider)) return res.sendStatus(204)

    providers.push(provider)
    
    res.sendStatus(201)
})

const server = app.listen(port, host, () => {
    console.log(`App listening at http://${host}:${port}`)
  })

function registered(provider) {
    const sameUrl = providers.filter(obj => obj.url === provider.url)
    const register = sameUrl.filter(obj => obj.type === provider.type)
    return register.length !== 0
}

function gracefull() {
    console.info('signal received.');
    console.log('Closing http server.');
    server.close(() => {
        console.log('Http server closed.');
        process.exit(0);
    });
}

process.on('SIGTERM', gracefull)
process.on('SIGINT', gracefull)